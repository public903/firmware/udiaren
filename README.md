# REGULÁTOR TEPLOTY V UDIARNI

> **Hardware:**
> ESP32
> krokový motor 200 krokový
> budič krokového motora: 4 mikrokroky
> tepelné čidlo DS18B20

**Kalibrácia: pri zapnutí regulátora musia byť dvierka ZATVORENÉ**

**Popis:**
Regulácia prebieha motorovým posunom dvierok.
Na reguláciu je použitý proporcionálny regulátor. Krokový motor cez kladku posúva dvierka pece. Motor je nastavený na 90° rozsah:
0° - zavreté dvierka
90° - otvorené dvierka
Rozsah otáčania motora sa môže zmeniť premennou PID range, čo je vlastne počet krokov motora, ktoré bude mať regulátor k dispozícii na vykonávanie regulácie.
800 krokov = 360° uhol otočenia motora. Pre 90° uhol teda zodpovedá 200 krokov.
Konštantou PID Kp sa nastavuje citlivosť regulátora.
Regulátor sa aktualizuje každých 5s.

