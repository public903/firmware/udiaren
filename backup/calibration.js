// Get current GPIO states when the page loads  
window.addEventListener('load', getCalTime);

// get calibration times
function getCalTime() {
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var myObj = JSON.parse(this.responseText);
      console.log(myObj);
      document.getElementById("caltimeopen").innerHTML = myObj.calibration.open;
      console.log(myObj.calibration.open);
    }
  }; 
  xhr.open("GET", "/caltime", true);
  xhr.send();
}

// Send Requests to calibrate
function btnCalibrate (element) {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "/upd?button="+element.id, true);
  xhr.send();
  if (element.id=="calopen") document.getElementById("calstate").innerHTML = "Otváranie";
  else if (element.id=="calclose") document.getElementById("calstate").innerHTML = "Zatváranie";
  else if (element.id=="calstop") {
      document.getElementById("calstate").innerHTML = "Stop";
      getCalTime();
    }
}

