/*********
  Rui Santos
  Complete instructions at https://RandomNerdTutorials.com/build-web-servers-ebook/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files.
  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*********/

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "LittleFS.h"
#include <AsyncElegantOTA.h>
#include <ESP8266mDNS.h>
#include <Arduino_JSON.h>

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Search for parameter in HTTP POST request
const char* PARAM_INPUT_1 = "ssid";
const char* PARAM_INPUT_2 = "pass";
const char* PARAM_INPUT_3 = "ip";

//Input Parameters
const char* PARAM_INPUT_OUTPUT = "output";
const char* PARAM_INPUT_STATE = "state";
const char* PARAM_INPUT_BUTTON = "button";
const char* PARAM_INPUT_TEMP = "temp";

// Set number of outputs
#define NUM_OUTPUTS  1

// Array with outputs you want to control
int outputGPIOs[NUM_OUTPUTS] = {2};

//Variables to save values from HTML form
String ssid;
String pass;
String ip;
String temperature;

// File paths to save input values permanently
const char* ssidPath = "/ssid.txt";
const char* passPath = "/pass.txt";
const char* ipPath = "/ip.txt";
const char* tempPath = "/temp.txt";

IPAddress localIP;
//IPAddress localIP(192, 168, 1, 200); // hardcoded

// Set your Gateway IP address
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 0, 0);

// Timer variables
unsigned long previousMillis = 0;
const long interval = 10000;  // interval to wait for Wi-Fi connection (milliseconds)

// Set LED GPIO
const int openPin = 2;
const int closePin = 1;
// Stores LED state
//String ledState;

boolean restart = false;

boolean triggerCalOpen=false, triggerCalClose=false, triggerCalStop=false;

// Initialize LittleFS
void initFS() {
  if (!LittleFS.begin()) {
    Serial.println("An error has occurred while mounting LittleFS");
  }
  Serial.println("LittleFS mounted successfully");
}

// Read File from LittleFS
String readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path, "r");
  if(!file || file.isDirectory()){
    Serial.println("- failed to open file for reading");
    return String();
  }

  String fileContent;
  while(file.available()){
    fileContent = file.readStringUntil('\n');
    break;
  }
  file.close();
  return fileContent;
}

// Write file to LittleFS
void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\r\n", path);

  File file = fs.open(path, "w");
  if(!file){
    Serial.println("- failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("- file written");
  } else {
    Serial.println("- frite failed");
  }
  file.close();
}

// Initialize WiFi
bool initWiFi() {
  if(ssid=="" || ip==""){
    Serial.println("Undefined SSID or IP address.");
    return false;
  }

  WiFi.mode(WIFI_STA);
  /*localIP.fromString(ip.c_str());

  if (!WiFi.config(localIP, gateway, subnet)){
    Serial.println("STA Failed to configure");
    return false;
  }*/
  WiFi.begin(ssid.c_str(), pass.c_str());

  Serial.println("Connecting to WiFi...");
  delay(20000);
  if(WiFi.status() != WL_CONNECTED) {
    Serial.println("Failed to connect.");
    return false;
  }

  Serial.println(WiFi.localIP());
  return true;
}

// Replaces placeholder with LED state value
/*String processor(const String& var) {
  String state;
  if(var == "DOOR") {
    if(digitalRead(openPin)) {
      state = "Otváranie";
    }
    else if (digitalRead(closePin)) {
      state = "Zatváranie";
    } 
    else {
      state = "Stop";
    }
    return state;
  }
  return String();
}*/

// Return JSON with Current Output States
String getOutputStates() {
  JSONVar myArray;
  for (int i =0; i<NUM_OUTPUTS; i++) {
    myArray["gpios"][i]["output"] = String(outputGPIOs[i]);
    myArray["gpios"][i]["state"] = String(digitalRead(outputGPIOs[i]));
    //myArray["gpios"][i] ="{\"output\":\"" + String(outputGPIOs[i]) + "\", \"state\": \"" + String(digitalRead(outputGPIOs[i])) + "\"}";
  }
  myArray["temp"] = temperature;
  String jsonString = JSON.stringify(myArray);
  Serial.print(jsonString);
  return jsonString;
}

void setup() {
  // Serial port for debugging purposes
  Serial.begin(115200);

  initFS();

  // Set GPIO 2 as an OUTPUT
  pinMode(openPin, OUTPUT);
  digitalWrite(openPin, LOW);
/*  pinMode(closePin, OUTPUT);
  digitalWrite(closePin, LOW);
*/
  // Load values saved in LittleFS
  ssid = readFile(LittleFS, ssidPath);
  pass = readFile(LittleFS, passPath);
  ip = readFile(LittleFS, ipPath);
  temperature = readFile(LittleFS, tempPath);
  Serial.println(ssid);
  Serial.println(pass);
  Serial.println(ip);
  Serial.println(temperature);

  if(initWiFi()) {
  	// Set up mDNS responder:
    if (!MDNS.begin("udiaren")) {
      Serial.println("Error setting up MDNS responder!");
      while (1) {
        delay(1000);
      }
    }

    // Route for root / web page
/*    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
      request->send(LittleFS, "/index.html", "text/html", false, processor);
    });
  
    server.serveStatic("/", LittleFS, "/");
*/
    // Route to set GPIO state to HIGH
/*    server.on("/open-manual", HTTP_GET, [](AsyncWebServerRequest *request) {
      digitalWrite(closePin, LOW);
      digitalWrite(openPin, HIGH);
      request->send(LittleFS, "/index.html", "text/html", false, processor);
    });

    // Route to set GPIO state to LOW
    server.on("/close-manual", HTTP_GET, [](AsyncWebServerRequest *request) {
      digitalWrite(openPin, LOW);
      digitalWrite(closePin, HIGH);
      request->send(LittleFS, "/index.html", "text/html", false, processor);
    });

    // Route to set GPIO state to LOW
    server.on("/stop-manual", HTTP_GET, [](AsyncWebServerRequest *request) {
      digitalWrite(openPin, LOW);
      digitalWrite(closePin, LOW);
      request->send(LittleFS, "/index.html", "text/html", false, processor);
    });
*/
  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(LittleFS, "/index.html", "text/html",false);
  });

  server.serveStatic("/", LittleFS, "/");

  server.on("/states", HTTP_GET, [](AsyncWebServerRequest *request) {
    String json = getOutputStates();
    request->send(200, "application/json", json);
    json = String();
  });

  //GET request to <ESP_IP>/update?output=<output>&state=<state>
  server.on("/upd", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String output;
    String state;
    // GET input1 value on <ESP_IP>/update?output=<output>&state=<state>
    if (request->hasParam(PARAM_INPUT_OUTPUT) && request->hasParam(PARAM_INPUT_STATE)) {
      output = request->getParam(PARAM_INPUT_OUTPUT)->value();
      state = request->getParam(PARAM_INPUT_STATE)->value();
      // Control GPIO
      digitalWrite(output.toInt(), state.toInt());
    }
    else if (request->hasParam(PARAM_INPUT_BUTTON)) {
      output = request->getParam(PARAM_INPUT_BUTTON)->value();
      if (output=="open") {
        digitalWrite(closePin, LOW);
        digitalWrite(openPin, HIGH);
      } 
      else if (output=="stop") {
        digitalWrite(openPin, LOW);
        digitalWrite(closePin, LOW);
      }
      else if (output=="close") {
        digitalWrite(openPin, LOW);
        digitalWrite(closePin, HIGH);
      }
      else if (output=="calopen") {
        digitalWrite(openPin, HIGH);
        digitalWrite(closePin, LOW);
        triggerCalOpen=true;
      }
      else if (output=="calstop") {
        digitalWrite(openPin, LOW);
        digitalWrite(closePin, LOW);
        triggerCalStop=true;
      }
      else if (output=="calclose") {
        digitalWrite(openPin, LOW);
        digitalWrite(closePin, HIGH);
        triggerCalClose=true;
      }
    }
    else {
      output = "No message sent";
      state = "No message sent";
    }
    Serial.print("GPIO: ");
    Serial.print(output);
    Serial.print(" - Set to: ");
    Serial.println(state);
    Serial.print("hasParam(PARAM_INPUT_BUTTON)=");
    Serial.println(request->hasParam(PARAM_INPUT_BUTTON));

    request->send(200, "text/plain", "OK");
  });

  server.on("/tmp", HTTP_POST, [](AsyncWebServerRequest *request) {
    int params = request->params();
    if (params) {
      AsyncWebParameter* p = request->getParam(0);
      if(p->isPost()){
        // HTTP POST input1 value
        if (p->name() == PARAM_INPUT_TEMP) {
          temperature = p->value().c_str();
          Serial.print("Temperature set to: ");
          Serial.println(temperature);
          // Write file to save value
          writeFile(LittleFS, tempPath, temperature.c_str());
        }
        Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
      }
    }
    request->send(LittleFS, "/index.html", "text/html");
  });

    server.on("/wifi", HTTP_POST, [](AsyncWebServerRequest *request) {  // Wifi manager form
      int params = request->params();
      for(int i=0;i<params;i++){
        AsyncWebParameter* p = request->getParam(i);
        if(p->isPost()){
          // HTTP POST ssid value
          if (p->name() == PARAM_INPUT_1) {
            ssid = p->value().c_str();
            Serial.print("SSID set to: ");
            Serial.println(ssid);
            // Write file to save value
            writeFile(LittleFS, ssidPath, ssid.c_str());
          }
          // HTTP POST pass value
          if (p->name() == PARAM_INPUT_2) {
            pass = p->value().c_str();
            Serial.print("Password set to: ");
            Serial.println(pass);
            // Write file to save value
            writeFile(LittleFS, passPath, pass.c_str());
          }
          // HTTP POST ip value
          if (p->name() == PARAM_INPUT_3) {
            ip = p->value().c_str();
            Serial.print("IP Address set to: ");
            Serial.println(ip);
            // Write file to save value
            writeFile(LittleFS, ipPath, ip.c_str());
          }
          //Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
        }
      }
      restart = true;
      request->send(200, "text/html", "<h1>Done. ESP will restart, connect to your router and go to IP address: " + ip + "</h1>");
    });

    AsyncElegantOTA.begin(&server);    // Start ElegantOTA
    server.begin();
  }
  else {
    // Connect to Wi-Fi network with SSID and password
    Serial.println("Setting AP (Access Point)");
    // NULL sets an open Access Point
    WiFi.softAP("Udiaren", NULL);

    IPAddress IP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(IP); 

    // Web Server Root URL
    server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(LittleFS, "/wifimanager.html", "text/html");
    });

    server.serveStatic("/", LittleFS, "/");

    server.on("/wifi", HTTP_POST, [](AsyncWebServerRequest *request) {  // Wifi manager form
      int params = request->params();
      for(int i=0;i<params;i++){
        AsyncWebParameter* p = request->getParam(i);
        if(p->isPost()){
          // HTTP POST ssid value
          if (p->name() == PARAM_INPUT_1) {
            ssid = p->value().c_str();
            Serial.print("SSID set to: ");
            Serial.println(ssid);
            // Write file to save value
            writeFile(LittleFS, ssidPath, ssid.c_str());
          }
          // HTTP POST pass value
          if (p->name() == PARAM_INPUT_2) {
            pass = p->value().c_str();
            Serial.print("Password set to: ");
            Serial.println(pass);
            // Write file to save value
            writeFile(LittleFS, passPath, pass.c_str());
          }
          // HTTP POST ip value
          if (p->name() == PARAM_INPUT_3) {
            ip = p->value().c_str();
            Serial.print("IP Address set to: ");
            Serial.println(ip);
            // Write file to save value
            writeFile(LittleFS, ipPath, ip.c_str());
          }
          //Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
        }
      }
      restart = true;
      request->send(200, "<h1>text/html", "Done. ESP will restart, connect to your router and go to IP address: " + ip + "</h1>");
    });
    AsyncElegantOTA.begin(&server);    // Start ElegantOTA
    server.begin();
  }
}

void loop() {
  MDNS.update();
  AsyncElegantOTA.loop();
  if(restart){
    delay(5000);
    ESP.restart();
  }
}
