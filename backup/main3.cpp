/*********
  Rui Santos
  Complete instructions at https://RandomNerdTutorials.com/build-web-servers-ebook/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files.
  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*********/

#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "SPIFFS.h"
#include <AsyncElegantOTA.h>
#include <ESPmDNS.h>
#include <Arduino_JSON.h>
#include <wifimanager.h>
#include "stepper.h"
#include "config.h"
#include <esp_task_wdt.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <PID_v1.h>
#include <ThingSpeak.h>
#include <HTTPClient.h>

#define DEBUG

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Create Motor object
STEPPER_DRV8825 motor;
#define OPEN  true
#define CLOSE false

// Temperature sensor DS18B20
OneWire oneWire(oneWireBus);
DallasTemperature ds18b20(&oneWire);

// Thingspeak
unsigned long myChannelNumber = 796611;
const char * myWriteAPIKey = "WVUXK2MSHJOWGK91";

//Input Parameters
const char* PARAM_INPUT_PID = "pid";
const char* PARAM_INPUT_OUTPUT = "output";
const char* PARAM_INPUT_STATE = "state";
const char* PARAM_INPUT_BUTTON = "button";
const char* PARAM_INPUT_TEMP = "temp";

// Set number of outputs
#define NUM_OUTPUTS  1

// Array with outputs you want to control
int outputGPIOs[NUM_OUTPUTS] = {2};

String temperatureSet, temperatureCurrent, calibVal;
float temperature;

const char* tempSetPath = "/tempset.txt";
const char* calOpenSetPath = "/calibval.txt";

// Timer variables
//uint32_t previousMillis = 0;
uint16_t tmrMeasure = 0, tmrDsWait = 0, tmrHalfSec = 0;
#define DS_TIME_WAIT  1000

boolean calOpen=false, tmrDsWaitOver=false;

// PID regulator
//Define Variables we'll be connecting to
double pidSetpoint, pidInput, pidOutput;
//Specify the links and initial tuning parameters
double Kp=KP, Ki=0, Kd=0;
PID udiPID(&pidInput, &pidOutput, &pidSetpoint, Kp, Ki, Kd, DIRECT);

// Return JSON with Current Output States
String getOutputStates() {
  JSONVar myArray;
/*  for (int i =0; i<NUM_OUTPUTS; i++) {
    myArray["gpios"][i]["output"] = String(outputGPIOs[i]);
    myArray["gpios"][i]["state"] = String(digitalRead(outputGPIOs[i]));
    //myArray["gpios"][i] ="{\"output\":\"" + String(outputGPIOs[i]) + "\", \"state\": \"" + String(digitalRead(outputGPIOs[i])) + "\"}";
  }
*/
  myArray["temp"]["set"] = temperatureSet;
  myArray["temp"]["current"] = temperatureCurrent;
  myArray["calibration"]["open"] = calibVal;
  myArray["pid"] = udiPID.GetMode();
  
  String jsonString = JSON.stringify(myArray);
  Serial.print(jsonString);
  return jsonString;
}

// Get calibration times
String getCalTimes() {
  JSONVar myArray;
  myArray["calibration"]["open"] = calibVal;
  String jsonString = JSON.stringify(myArray);
  Serial.print(jsonString);
  return jsonString;
}

// Kalibracia vychodzej polohy  dvierok
void motorCalibration(void) {
  motor.setDir(CLOSE);
  if (motor.getStepsCounter() < 0) motor.resetCounter();
  motor.start(motor.getStepsCounter() + (calibVal.toInt() / 10));
  while (motor.getRun()) {
    motor.run();
  }
  motor.resetCounter();
}

// Konverzia vystupu PID regulatora na motor-kroky
uint32_t outToMotor(int8_t outPid, uint32_t allSteps) { // vystup z PID , pocet krokov motora pre cely regulacny rozsah
  return(allSteps * outPid / 100);
}
/*********************************************************/
/*********************************************************/
void setup() {
  // Serial port for debugging purposes
  Serial.begin(115200);
  
  ds18b20.begin();
  ds18b20.requestTemperatures();
  temperature = ds18b20.getTempCByIndex(0);
  if (temperature != -127) temperatureCurrent = String(temperature);
  ds18b20.setWaitForConversion(false);  

  esp_task_wdt_init(WDT_TIMEOUT, false); //enable panic so ESP32 restarts

  initSPIFFS();

  // Load values saved in SPIFFS
  ssid = readFile(SPIFFS, ssidPath);
  pass = readFile(SPIFFS, passPath);
  ip = readFile(SPIFFS, ipPath);
  temperatureSet = readFile(SPIFFS, tempSetPath);
  calibVal = readFile(SPIFFS, calOpenSetPath);
  wifiScanJson = String();
  Serial.println(ssid);
  Serial.println(pass);
  Serial.println(ip);
  Serial.println(temperatureSet);
  Serial.printf("Calibration value: %s \n", calibVal.c_str());

  // PID
  udiPID.SetSampleTime(PID_SAMPLE_TIME);
  udiPID.SetOutputLimits(0,PID_RANGE);
  udiPID.SetMode(AUTOMATIC);
  pidInput = temperature;
  pidSetpoint = temperatureSet.toInt();

  // Motor
  motor.begin(ENABLE_PIN, DIR_PIN, STEP_PIN, MOTOR_STEPS, MICROSTEPS);
  motor.setRpm(RPM);
  motor.resetCounter();   // kalibracia dvierok po resete. Dvierka musia byt po resete zavrete.
  //motorCalibration();

  if(initWiFi()) {
    // Set up mDNS responder:
    if (!MDNS.begin("udiaren")) {
      Serial.println("Error setting up MDNS responder!");
      while (1) {
        delay(1000);
      }
    }
  }
  else {
    // Connect to Wi-Fi network with SSID and password
    Serial.println("Setting AP (Access Point)");
    // NULL sets an open Access Point
    WiFi.softAP("Udiaren", NULL);

    IPAddress IP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(IP); 
  }

  //ThingSpeak.begin() );  // Initialize ThingSpeak

  // Route for root / web page
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/index.html", "text/html",false);
  });

  server.serveStatic("/", SPIFFS, "/");

  server.on("/states", HTTP_GET, [](AsyncWebServerRequest *request) {
    String json = getOutputStates();
    request->send(200, "application/json", json);
    json = String();
  });

  //GET request to <ESP_IP>/update?output=<output>&state=<state>
  server.on("/upd", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String output;
    // GET input1 value on <ESP_IP>/update?output=<output>&state=<state>
    if (request->hasParam(PARAM_INPUT_PID)) {
      output = request->getParam(PARAM_INPUT_PID)->value();
      udiPID.SetMode(output.toInt());
    }
    else if (request->hasParam(PARAM_INPUT_BUTTON)) {
      output = request->getParam(PARAM_INPUT_BUTTON)->value();
      if (output=="open") {
        motor.setDir(true);
        motor.start();
      } 
      else if (output=="stop") {
        motor.stop();
      }
      else if (output=="close" || output=="calclose") {
        motor.setDir(false);
        motor.start();
      }
      else if (output=="calopen") {
        motor.resetCounter();
        motor.setDir(true);
        motor.start();
        calOpen = true;
      }
      else if (output=="calstop") {
        motor.stop();
        if (calOpen) {
           calibVal = motor.getStepsCounter();
           calOpen = false;
        }
        writeFile(SPIFFS, calOpenSetPath, calibVal.c_str());
        Serial.print("Calibration value of steps is: ");
        Serial.println(calibVal.c_str());
      }
    }
    else {
      output = "No message sent";
    }
    request->send(200, "text/plain", "OK");
  });

  server.on("/tmp", HTTP_POST, [](AsyncWebServerRequest *request) {
    int params = request->params();
    if (params) {
      AsyncWebParameter* p = request->getParam(0);
      if(p->isPost()){
        // HTTP POST input1 value
        if (p->name() == PARAM_INPUT_TEMP) {
          temperatureSet = p->value().c_str();
          writeFile(SPIFFS, tempSetPath, temperatureSet.c_str());
        }
        Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
      }
    }
    request->send(SPIFFS, "/index.html", "text/html");
  });

  // Calibration time
  server.on("/caltime", HTTP_GET, [](AsyncWebServerRequest *request) {
    String json = getCalTimes();
    request->send(200, "application/json", json);
    json = String();
  });

  // Wifi scan
  server.on("/wifiscan", HTTP_GET, [](AsyncWebServerRequest *request) {
    printScanResult(WiFi.scanNetworks());
    request->send(200, "application/json", wifiScanJson);
    wifiScanJson = String();
  });

  server.on("/wifi", HTTP_POST, [](AsyncWebServerRequest *request) {  // Wifi manager form
    int params = request->params();
    for(int i=0;i<params;i++){
      AsyncWebParameter* p = request->getParam(i);
      if(p->isPost()){
        // HTTP POST ssid value
        if (p->name() == PARAM_INPUT_1) {
          ssid = p->value().c_str();
          Serial.print("SSID set to: ");
          Serial.println(ssid);
          // Write file to save value
          writeFile(SPIFFS, ssidPath, ssid.c_str());
        }
        // HTTP POST pass value
        if (p->name() == PARAM_INPUT_2) {
          pass = p->value().c_str();
          Serial.print("Password set to: ");
          Serial.println(pass);
          // Write file to save value
          writeFile(SPIFFS, passPath, pass.c_str());
        }
        // HTTP POST ip value
        if (p->name() == PARAM_INPUT_3) {
          ip = p->value().c_str();
          Serial.print("IP Address set to: ");
          Serial.println(ip);
          // Write file to save value
          writeFile(SPIFFS, ipPath, ip.c_str());
        }
        //Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
      }
    }
    restart = true;
    request->send(200, "text/html", "<h1>Done. ESP will restart, connect to your router and go to IP address: " + ip + "</h1>");
  });
  AsyncElegantOTA.begin(&server);    // Start ElegantOTA
  server.begin();
}
/*********************************************************/
/*********************************************************/
void loop() {
  esp_task_wdt_reset();
  AsyncElegantOTA.loop();
  if(restart){
    delay(5000);
    ESP.restart();
  }

  motor.run();
  udiPID.Compute();

  if ((uint16_t)((uint16_t)millis() - tmrHalfSec) >= 500) { // 0.5s timer
    tmrHalfSec += 500;
    #ifdef DEBUG
      Serial.print("PID.getMode()=");Serial.println(udiPID.GetMode());
    #endif
    if (udiPID.GetMode()) {   // Automatika
      // int32_t move = outToMotor(pidOutput, calibVal.toInt()) - motor.getStepsCounter();  // kalibracna hodnota sa pouzije z webu
      if (!motor.getRun()) {
        int32_t move = pidOutput - motor.getStepsCounter();
        if (move > 0) motor.setDir(true);
        else motor.setDir(false);
        motor.start(abs(move));
      }
    }
  }
  if ((uint16_t)((uint16_t)millis() - tmrMeasure) >= MEASURE_TIME) { // measure timer
    tmrMeasure += MEASURE_TIME;
    tmrDsWait = millis();
    tmrDsWaitOver = false;
    ds18b20.requestTemperatures();
    Serial.println("Measure timer");
  }
  if (((uint16_t)((uint16_t)millis() - tmrDsWait) >= DS_TIME_WAIT) && !tmrDsWaitOver) { // DS18B20 request timer
    tmrDsWaitOver = true;
    temperature = ds18b20.getTempCByIndex(0);
    if (temperature != -127) {
      temperatureCurrent = String(temperature,1);
      pidInput = temperature * 10;
    }
    pidSetpoint = temperatureSet.toInt() * 10;

    #ifdef DEBUG
      Serial.print("getStepsCounter=");Serial.println(motor.getStepsCounter());
      Serial.print("pidInput=");Serial.println(pidInput);
      Serial.print("pidSetpoint=");Serial.println(pidSetpoint);
      Serial.print("pidOutput=");Serial.println(pidOutput);
    #endif
  }
}
