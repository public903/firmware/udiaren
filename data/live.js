// Get current GPIO states when the page loads  
window.addEventListener('load', getStates);

// Function to get and update GPIO states on the webpage when it loads for the first time
function getStates(){
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var myObj = JSON.parse(this.responseText);
      console.log(myObj);
      document.getElementById("tempSet").innerHTML = myObj.temp.set;
      document.getElementById("tempCur").innerHTML = myObj.temp.current;
      document.getElementById("pidRL").innerHTML = myObj.pid.range;
      document.getElementById("pidKpL").innerHTML = myObj.pid.kp;
      document.getElementById("pidOL").innerHTML = myObj.pid.output;
    }
  }; 
  xhr.open("GET", "/states", true);
  xhr.send();
}

