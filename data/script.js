// Get current GPIO states when the page loads  
window.addEventListener('load', getStates);

// Function to get and update GPIO states on the webpage when it loads for the first time
function getStates(){
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var myObj = JSON.parse(this.responseText);
      console.log(myObj);
      document.getElementById("tempSet").innerHTML = myObj.temp.set;
      document.getElementById("tempCur").innerHTML = myObj.temp.current;
      document.getElementById("pidRL").innerHTML = myObj.pid.range;
      document.getElementById("pidKpL").innerHTML = myObj.pid.kp;
      document.getElementById("tsChNumL").innerHTML = myObj.thingSpeak.chNum;
      document.getElementById("tsApiL").innerHTML = myObj.thingSpeak.api;
      if (myObj.pid.mode == 1) {
        document.getElementById("pidM").innerHTML = "AUTOMATIC";
        document.getElementById("swAut").checked = true;
      }
      else {
        document.getElementById("pidM").innerHTML = "MANUAL";
        document.getElementById("swAut").checked = false;
      }
      console.log(myObj.temp.current);
    }
  }; 
  xhr.open("GET", "/states", true);
  xhr.send();
}

// Send Requests to Control GPIOs
function toggleCheckbox (element) {
  var xhr = new XMLHttpRequest();
  if (element.checked) {
    xhr.open("GET", "/upd?pid=1", true);
    document.getElementById("pidM").innerHTML = "AUTOMATIC";
  }
  else {
    xhr.open("GET", "/upd?pid=0", true);
    document.getElementById("pidM").innerHTML = "MANUAL"; 
  }
  xhr.send();
}

// Send Requests to Control GPIOs
function btnManual (element) {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", "/upd?button="+element.id, true);
  if (element.id=="open") document.getElementById("doorstate").innerHTML = "Dvierka sú otvorené";
  else if (element.id=="close") document.getElementById("doorstate").innerHTML = "Dvierka sú zatvorené";
  xhr.send();
  getStates();
}

