// Get current WIFI scan states when the page loads  
window.addEventListener('load', getWifiScan);

// Function to get and update GPIO states on the webpage when it loads for the first time
function getWifiScan(){
  var xhr = new XMLHttpRequest();
  xhr.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var myObj = JSON.parse(this.responseText);
      var wifihtml = "";
      console.log(myObj);
      for (i in myObj.wifi) {
        var ssid = myObj.wifi[i].ssid;
        var channel = myObj.wifi[i].channel;
        var rssi = myObj.wifi[i].rssi;
        var encryption = myObj.wifi[i].encryption;
        console.log(ssid);
        console.log(channel);
        console.log(rssi);
        console.log(encryption);
        wifihtml += "<button class=\"btn\" onclick=\"document.getElementById(\'ssid\').value=\'"+ssid+"\'\">"+ssid+"</button><br>";
      }
      document.getElementById("wifiscan").innerHTML = wifihtml;
    }
  }; 
  xhr.open("GET", "/wifiscan", true);
  xhr.send();
}
  