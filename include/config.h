#ifndef BOARD_H
#define BOARD_H

#include <Arduino.h>

#define WDT_TIMEOUT     20
#define oneWireBus      13
#define MEASURE_TIME    5000
#define THINGSPEAK_TMR    20000

/* PID */
#define PID_SAMPLE_TIME MEASURE_TIME

/* ThingSpeak */
#define SECRET_CH_ID    796611
#define SECRET_WRITE_APIKEY "WVUXK2MSHJOWGK91"

#endif  // BOARD_H