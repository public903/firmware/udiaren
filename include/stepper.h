/* 
 * Library for stepper motor driver DRV8825
 * 
 * MOTOR STEPS = 200
 * MICROSTEPS = 4
 * WIRE: ENABLE, DIR, STEP
 * RPM: 0 - 120
 * 
 * Author: Trnik 23.7.2020
 * 
*/

#ifndef STEPPER_H_
#define STEPPER_H_

#include <Arduino.h>
#include <stdio.h>

// MOTOR
#define MOTOR_STEPS 200L
#define DIR_PIN     14
#define STEP_PIN    12
#define ENABLE_PIN  27
#define MICROSTEPS  4L
#define RPM         10

class STEPPER_DRV8825 {
public:
    STEPPER_DRV8825();
    void begin(uint8_t enable, uint8_t dir, uint8_t step, uint16_t mot_stps, uint8_t micstps);
    void run(void);
    void setRpm(uint8_t rpm);
    void start(uint32_t moveSteps);
    void start(void);
    void setPause(bool ppause);
    void stop(void);
    int32_t getStepsCounter(void);
    void resetCounter(void);
    void setDir(bool sdir);
    bool getDir(void);
    bool getRun(void);
    void enable(void);
    void disable(void);

private:
    uint32_t previousMicros, move;
    int32_t counter;
    uint16_t stepInterval;
    uint8_t pinEn, pinDir, pinSt, rpm;
    uint16_t motorSteps;
    uint8_t microsteps;
    bool dir, pause;
};

#endif