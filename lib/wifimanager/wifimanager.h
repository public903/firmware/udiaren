/* WiFi Manager
*/
#include <Arduino.h>
#include <WiFi.h>
#include "SPIFFS.h"

#ifndef wifimanager_h
#define wifimanager_h

// Search for parameter in HTTP POST request
const char* PARAM_INPUT_1 = "ssid";
const char* PARAM_INPUT_2 = "pass";
const char* PARAM_INPUT_3 = "ip";

//Variables to save values from HTML form
String ssid;
String pass;
String ip;
String wifiScanJson;

// File paths to save input values permanently
const char* ssidPath = "/ssid.txt";
const char* passPath = "/pass.txt";
const char* ipPath = "/ip.txt";

boolean restart = false;

//IPAddress localIP;
//IPAddress localIP(192, 168, 1, 200); // hardcoded

// Set your Gateway IP address
IPAddress gateway(192, 168, 0, 1);
IPAddress subnet(255, 255, 0, 0);

/*********************************************************/
/*********************************************************/
// Initialize SPIFFS
void initSPIFFS() {
if (!SPIFFS.begin(true)) {
Serial.println("An error has occurred while mounting SPIFFS");
}
Serial.println("SPIFFS mounted successfully");
}
/*********************************************************/
// Read File from LittleFS
String readFile(fs::FS &fs, const char * path) {
  Serial.printf("Reading file: %s\r\n", path);

  File file = fs.open(path, "r");
  if(!file || file.isDirectory()){
    Serial.println("- failed to open file for reading");
    return String();
  }

  String fileContent;
  while(file.available()){
    fileContent = file.readStringUntil('\n');
    break;
  }
  file.close();
  return fileContent;
}
/*********************************************************/
// Write file to LittleFS
void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\r\n", path);

  File file = fs.open(path, "w");
  if(!file){
    Serial.println("- failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("- file written");
  } else {
    Serial.println("- file failed");
  }
  file.close();
}
/*********************************************************/
// Initialize WiFi
bool initWiFi() {
  if(ssid==""){
    Serial.println("Undefined SSID.");
    return false;
  }

  WiFi.mode(WIFI_STA);
  //localIP.fromString(ip.c_str());

/*  if (!WiFi.config(localIP, gateway, subnet)){
    Serial.println("STA Failed to configure");
    WiFi.mode(WIFI_AP);
    return false;
  }*/
  WiFi.begin(ssid.c_str(), pass.c_str());

  Serial.println("Connecting to WiFi...");
  
  for (uint8_t i=0; i<20; i++) {
    delay(1000);
    if (WiFi.status() == WL_CONNECTED) i=20;
  }

  if(WiFi.status() != WL_CONNECTED) {
    Serial.println("Failed to connect.");
    WiFi.mode(WIFI_AP);
    return false;
  }

  Serial.println(WiFi.localIP());
  return true;
}
/*********************************************************/
void printScanResult(int networksFound)
{
  Serial.printf("%d network(s) found\n", networksFound);
  JSONVar myArray;
  for (int i = 0; i < networksFound; i++)
  {
    Serial.printf("%d: %s, Ch:%d (%ddBm) %s\n", i + 1, WiFi.SSID(i).c_str(), WiFi.channel(i), WiFi.RSSI(i), WiFi.encryptionType(i) == WIFI_AUTH_OPEN ? "open" : "lock");
    myArray["wifi"][i]["ssid"] = WiFi.SSID(i).c_str();
    myArray["wifi"][i]["channel"] = WiFi.channel(i);
    myArray["wifi"][i]["rssi"] = WiFi.RSSI(i);
    myArray["wifi"][i]["encryption"] = WiFi.encryptionType(i) == WIFI_AUTH_OPEN ? "open" : "lock";
  }
  wifiScanJson = JSON.stringify(myArray);
}
/*********************************************************/


#endif