/*********
  Rui Santos
  Complete instructions at https://RandomNerdTutorials.com/build-web-servers-ebook/
  
  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files.
  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*********/

#include <Arduino.h>
#include <WiFi.h>
#include <AsyncTCP.h>
#include <ESPAsyncWebServer.h>
#include "SPIFFS.h"
#include <AsyncElegantOTA.h>
#include <ESPmDNS.h>
#include <Arduino_JSON.h>
#include <wifimanager.h>
#include "stepper.h"
#include "config.h"
#include <esp_task_wdt.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <PID_v1.h>
#include "ThingSpeak.h"
#include <HTTPClient.h>
#include "secrets.h"

#define DEBUG

// Create AsyncWebServer object on port 80
AsyncWebServer server(80);

// Create Motor object
STEPPER_DRV8825 motor;
#define OPEN  true
#define CLOSE false

// Temperature sensor DS18B20
OneWire oneWire(oneWireBus);
DallasTemperature ds18b20(&oneWire);

// Thingspeak
unsigned long myChannelNumber = SECRET_CH_ID;
const char * myWriteAPIKey = SECRET_WRITE_APIKEY;
int keyIndex = 0;            // your network key Index number (needed only for WEP)
WiFiClient  client;

//Input Parameters
const char* PARAM_INPUT_OUTPUT = "output";
const char* PARAM_INPUT_STATE = "state";
const char* PARAM_INPUT_BUTTON = "button";
const char* PARAM_INPUT_TEMP = "temp";
const char* PARAM_INPUT_PID = "pid";
const char* PARAM_INPUT_PIDKP = "pidKp";
const char* PARAM_INPUT_PIDRANGE = "pidRange";
const char* PARAM_INPUT_TSCHN = "tsChNum";
const char* PARAM_INPUT_TSAPI = "tsApi";

String temperatureSet, temperatureCurrent, pidKpStr, pidRangeStr, tsChNumStr, tsApiStr;
float temperature;

const char* tempSetPath = "/tempset.txt";
const char* pidKpPath = "/pidkp.txt";
const char* pidRangePath = "/pidrange.txt";
const char* tsChNumPath = "/tschnum.txt";
const char* tsApiPath = "/tsapi.txt";

// Timer variables
uint16_t tmrMeasure = 0, tmrDsWait = 0, tmrHalfSec = 0, tmrThingSpeak = 0;
#define DS_TIME_WAIT  1000

boolean calOpen=false, tmrDsWaitOver=false;

// PID regulator
//Define Variables we'll be connecting to
double pidSetpoint, pidInput, pidOutput;
//Specify the links and initial tuning parameters
double Kp=0, Ki=0, Kd=0;


PID udiPID(&pidInput, &pidOutput, &pidSetpoint, Kp, Ki, Kd, DIRECT);

// Return JSON with Current Output States
String getOutputStates() {
  JSONVar myArray;
  myArray["temp"]["set"] = temperatureSet;
  myArray["temp"]["current"] = temperatureCurrent;
  myArray["pid"]["mode"] = (String)udiPID.GetMode();
  myArray["pid"]["kp"] = (String)udiPID.GetKp();
  myArray["pid"]["range"] = pidRangeStr;
  myArray["pid"]["output"] = (String)pidOutput;
  myArray["thingSpeak"]["chNum"] = (String)myChannelNumber;
  myArray["thingSpeak"]["api"] = myWriteAPIKey;
  
  String jsonString = JSON.stringify(myArray);
  #ifdef DEBUG
    Serial.print("getOutputStates: ");
    Serial.print(jsonString);
  #endif
  return jsonString;
}

/*********************************************************/
/*********************************************************/
void setup() {
  // Serial port for debugging purposes
  Serial.begin(115200);
  
  ds18b20.begin();
  ds18b20.requestTemperatures();
  temperature = ds18b20.getTempCByIndex(0);
  if (temperature != -127) temperatureCurrent = String(temperature);
  //ds18b20.setWaitForConversion(false);  

  esp_task_wdt_init(WDT_TIMEOUT, false); //enable panic so ESP32 restarts

  initSPIFFS();

  // Load values saved in SPIFFS
  ssid = readFile(SPIFFS, ssidPath);
  pass = readFile(SPIFFS, passPath);
  ip = readFile(SPIFFS, ipPath);
  temperatureSet = readFile(SPIFFS, tempSetPath);
  pidKpStr = readFile(SPIFFS, pidKpPath);
  pidRangeStr = readFile(SPIFFS, pidRangePath);
  wifiScanJson = String();
  #ifdef DEBUG
    Serial.println(ssid);
    Serial.println(pass);
    Serial.println(ip);
    Serial.println(temperatureSet);
    Serial.println(pidKpStr);
    Serial.println(pidRangeStr);
  #endif

  // PID
  udiPID.SetSampleTime(PID_SAMPLE_TIME);
  udiPID.SetOutputLimits(0, pidRangeStr.toInt());
  udiPID.SetMode(AUTOMATIC);
  Kp = pidKpStr.toInt();
  udiPID.SetTunings(Kp, Ki, Kd);
  pidInput = temperatureCurrent.toDouble() * 10;
  pidSetpoint = temperatureSet.toInt() * 10;
  #ifdef DEBUG
    Serial.print("pidKpStr=");Serial.println(pidKpStr);
    Serial.print("pidKpStr.toInt=");Serial.println(pidKpStr.toInt());
    Serial.print("Kp=");Serial.println(Kp);
  #endif

  // Motor
  motor.begin(ENABLE_PIN, DIR_PIN, STEP_PIN, MOTOR_STEPS, MICROSTEPS);
  motor.setRpm(RPM);
  motor.resetCounter();   // kalibracia dvierok po resete. Dvierka musia byt po resete zavrete.

  if(initWiFi()) {
    // Set up mDNS responder:
    if (!MDNS.begin("udiaren")) {
      Serial.println("Error setting up MDNS responder!");
      while (1) {
        delay(1000);
      }
    }
    ThingSpeak.begin(client);  // Initialize ThingSpeak
  }
  else {
    // Connect to Wi-Fi network with SSID and password
    Serial.println("Setting AP (Access Point)");
    // NULL sets an open Access Point
    WiFi.softAP("Udiaren", NULL);

    IPAddress IP = WiFi.softAPIP();
    Serial.print("AP IP address: ");
    Serial.println(IP); 
  }
  
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send(SPIFFS, "/index.html", "text/html",false);
  });

  server.serveStatic("/", SPIFFS, "/");

  server.on("/states", HTTP_GET, [](AsyncWebServerRequest *request) {
    String json = getOutputStates();
    request->send(200, "application/json", json);
    json = String();
  });

  server.on("/upd", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String output;
    if (request->hasParam(PARAM_INPUT_PID)) {
      output = request->getParam(PARAM_INPUT_PID)->value();
      udiPID.SetMode(output.toInt());
    }
    else if (request->hasParam(PARAM_INPUT_BUTTON)) {
      output = request->getParam(PARAM_INPUT_BUTTON)->value();
      if (output=="open") {
        int32_t move = pidRangeStr.toInt() - motor.getStepsCounter();
        if (move > 0) motor.setDir(true);
        else motor.setDir(false);
        motor.start(abs(move));
      } 
      else if (output=="close" || output=="calclose") {
        int32_t move = 0 - motor.getStepsCounter();
        if (move > 0) motor.setDir(true);
        else motor.setDir(false);
        motor.start(abs(move));
      }
    }
    else if (request->hasParam(PARAM_INPUT_PID)) {
      output = request->getParam(PARAM_INPUT_PID)->value();
      udiPID.SetMode(output.toInt());
    }
    else
    {
      output = "No message sent";
    }
    request->send(200, "text/plain", "OK");
  });

  server.on("/tmp", HTTP_POST, [](AsyncWebServerRequest *request) {
    int params = request->params();
    if (params) {
      AsyncWebParameter* p = request->getParam(0);
      if(p->isPost()){
        if (p->name() == PARAM_INPUT_TEMP) {
          temperatureSet = p->value().c_str();
          writeFile(SPIFFS, tempSetPath, temperatureSet.c_str());
        }
        #ifdef DEBUG
          Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
        #endif
      }
    }
    request->send(SPIFFS, "/index.html", "text/html");
  });

  server.on("/pid", HTTP_POST, [](AsyncWebServerRequest *request) {
    int params = request->params();
    for(int i=0;i<params;i++){
      if (params) {
        AsyncWebParameter* p = request->getParam(i);
        if(p->isPost()){
          if (p->name() == PARAM_INPUT_PIDKP) {
            pidKpStr = p->value().c_str();
            writeFile(SPIFFS, pidKpPath, pidKpStr.c_str());
            Kp = pidKpStr.toInt();
            udiPID.SetTunings(Kp, Ki, Kd);
          }
          if (p->name() == PARAM_INPUT_PIDRANGE) {
            pidRangeStr = p->value().c_str();
            writeFile(SPIFFS, pidRangePath, pidRangeStr.c_str());
            udiPID.SetOutputLimits(0, pidRangeStr.toInt());
          }
          #ifdef DEBUG
            Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
            Serial.print("pidKpStr=");Serial.println(pidKpStr);
            Serial.print("pidKpStr.toInt=");Serial.println(pidKpStr.toInt());
            Serial.print("Kp=");Serial.println(Kp);
            Serial.print("pidRangeStr.toInt=");Serial.println(pidRangeStr.toInt());
          #endif
        }
      }
    }
    request->send(SPIFFS, "/index.html", "text/html");
  });

  server.on("/thingspeak", HTTP_POST, [](AsyncWebServerRequest *request) {
    int params = request->params();
    for(int i=0;i<params;i++){
      if (params) {
        AsyncWebParameter* p = request->getParam(i);
        if(p->isPost()){
          if (p->name() == PARAM_INPUT_TSCHN) {
            tsChNumStr = p->value().c_str();
            writeFile(SPIFFS, tsChNumPath, tsChNumStr.c_str());
            myChannelNumber = tsChNumStr.toInt();
          }
          if (p->name() == PARAM_INPUT_TSAPI) {
            myWriteAPIKey = p->value().c_str();
            writeFile(SPIFFS, tsApiPath, tsApiStr.c_str());
          }
        }
      }
    }
    request->send(SPIFFS, "/index.html", "text/html");
  });

  // Wifi scan
  server.on("/wifiscan", HTTP_GET, [](AsyncWebServerRequest *request) {
    //printScanResult(WiFi.scanNetworks());
    request->send(200, "application/json", wifiScanJson);
    wifiScanJson = String();
  });

  server.on("/wifi", HTTP_POST, [](AsyncWebServerRequest *request) {  // Wifi manager form
    int params = request->params();
    for(int i=0;i<params;i++){
      AsyncWebParameter* p = request->getParam(i);
      if(p->isPost()){
        if (p->name() == PARAM_INPUT_1) {
          ssid = p->value().c_str();
          #ifdef DEBUG
            Serial.print("SSID set to: ");
            Serial.println(ssid);
          #endif
          writeFile(SPIFFS, ssidPath, ssid.c_str());
        }
        if (p->name() == PARAM_INPUT_2) {
          pass = p->value().c_str();
          #ifdef DEBUG
            Serial.print("Password set to: ");
            Serial.println(pass);
          #endif
          writeFile(SPIFFS, passPath, pass.c_str());
        }
/*        if (p->name() == PARAM_INPUT_3) {
          ip = p->value().c_str();
          #ifdef DEBUG
            Serial.print("IP Address set to: ");
            Serial.println(ip);
          #endif
          writeFile(SPIFFS, ipPath, ip.c_str());
        }*/
        #ifdef DEBUG
          Serial.printf("POST[%s]: %s\n", p->name().c_str(), p->value().c_str());
        #endif
      }
    }
    restart = true;
    request->send(200, "text/html", "<h1>Done. ESP will restart, connect to your router and go to IP address: " + ip + "</h1>");
  });
  AsyncElegantOTA.begin(&server);    // Start ElegantOTA
  server.begin();
}
/*********************************************************/
/*********************************************************/
void loop() {
  esp_task_wdt_reset();
  AsyncElegantOTA.loop();
  if(restart){
    delay(5000);
    ESP.restart();
  }

  motor.run();

  if ((uint16_t)((uint16_t)millis() - tmrHalfSec) >= 500) { // 0.5s timer
    tmrHalfSec += 500;
    if (udiPID.GetMode()) {   // Automatika
      if (!motor.getRun()) {
        int32_t move = pidOutput - motor.getStepsCounter();
        if (move > 0) motor.setDir(true);
        else motor.setDir(false);
        motor.start(abs(move));
      }
    }
  }

  if ((uint16_t)((uint16_t)millis() - tmrMeasure) >= MEASURE_TIME) { // measure timer
    tmrMeasure += MEASURE_TIME;
    tmrDsWait = millis();
    tmrDsWaitOver = false;
    ds18b20.requestTemperatures();
    temperature = ds18b20.getTempCByIndex(0);

    if (temperature != -127) {
      temperatureCurrent = String(temperature,1);
      pidInput = temperatureCurrent.toDouble() * 10;
    }
    else temperatureCurrent = "N/A";
    pidSetpoint = temperatureSet.toInt() * 10;
    udiPID.Compute();
    #ifdef DEBUG
      Serial.print("temperatureCurrent=");Serial.println(temperatureCurrent);
      Serial.print("getStepsCounter=");Serial.println(motor.getStepsCounter());
      Serial.print("PID.getMode()=");Serial.println(udiPID.GetMode());
      Serial.print("pid Kp=");Serial.println(udiPID.GetKp());
      Serial.print("pidInput=");Serial.println(pidInput);
      Serial.print("pidSetpoint=");Serial.println(pidSetpoint);
      Serial.print("pidOutput=");Serial.println(pidOutput);
    #endif
  }

/*  if (((uint16_t)((uint16_t)millis() - tmrDsWait) >= DS_TIME_WAIT) && !tmrDsWaitOver) { // DS18B20 request timer
    tmrDsWaitOver = true;
    temperature = ds18b20.getTempCByIndex(0);
    if (temperature != -127) {
      temperatureCurrent = String(temperature,1);
      pidInput = temperatureCurrent.toDouble() * 10;
    }
    pidSetpoint = temperatureSet.toInt() * 10;
    udiPID.Compute();

    #ifdef DEBUG
      Serial.print("getStepsCounter=");Serial.println(motor.getStepsCounter());
      Serial.print("PID.getMode()=");Serial.println(udiPID.GetMode());
      Serial.print("pid Kp=");Serial.println(udiPID.GetKp());
      Serial.print("pidInput=");Serial.println(pidInput);
      Serial.print("pidSetpoint=");Serial.println(pidSetpoint);
      Serial.print("pidOutput=");Serial.println(pidOutput);
    #endif
  }
*/
  if ((uint16_t)((uint16_t)millis() - tmrThingSpeak) >= THINGSPEAK_TMR) { // measure timer
    tmrThingSpeak += THINGSPEAK_TMR;
    if (WiFi.getMode() & 1) {
      ThingSpeak.setField(1, temperatureCurrent);
      ThingSpeak.setField(2, (String)pidOutput);
      ThingSpeak.setField(3, temperatureSet);
      ThingSpeak.setField(4, pidKpStr);
      ThingSpeak.setField(5, pidRangeStr);

      int x = ThingSpeak.writeFields(myChannelNumber, myWriteAPIKey);
      if(x == 200){
        Serial.println("Channel update successful.");
      }
      else{
        Serial.println("Problem updating channel. HTTP error code " + String(x));
      }
    }
  }
}
